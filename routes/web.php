<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/login', function () {
    return view('auth.login');
});

Route::get('/register', function () {
    return view('auth.register');
});

Route::post('create', 'UserController@create');
Route::post('login', 'UserController@login');
Route::get('dashboard','UserController@dashboard');
Route::get('users','UserController@getallusers');
Route::get('profile','UserController@profile');
Route::get('editProfile', 'UserController@editProfile');
Route::get('commingsoon','UserController@commingsoon');
Route::get('approveStatus/{id}', 'UserController@approveStatus');
Route::get('rejectStatus/{id}', 'UserController@rejectStatus');
Route::get('deleteUser/{id}', 'UserController@deleteUser');
Route::get('logout', 'UserController@destroy');

// Route::get('auth/google', 'UserController@redirectToGoogle');
// Route::get('auth/google/callback', 'UserController@handleGoogleCallback');

Route::get('login/google', 'UserController@redirectToProvider');
Route::get('login/google/callback', 'UserController@handleProviderCallback');