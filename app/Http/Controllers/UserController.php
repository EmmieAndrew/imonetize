<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use Session;
// use Socialite;
use Auth;
use Exception;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $request->validate([
            'username' => 'required',
            'password' => 'required|min:6',
            'email' => 'required|email|unique:users'
        ], [
            'name.required' => 'Name is required',
            'password.required' => 'Password is required'
        ]);

        $user = new User();
        $user->name = $request->input('username');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->save();
        $this->registerMail($user->email);
        return view('auth.login')->with('success','You are logged in!');
    }

    function registerMail($email){
        $details = [
            'title' => 'Mail from Imonetize.com',
            'body' => 'Thanks for Registration on Imonetize Dashboard . You will get Verified soon'
        ];
        \Mail::to($email)->send(new \App\Mail\Mails($details));
    }

    function verificationMail($email){
        $details = [
            'title' => 'Mail from Imonetize.com',
            'body' => 'Hi '.$email.' You are verified successfully , you can login now'
        ];
        \Mail::to($email)->send(new \App\Mail\verificationMail($details));
    }

    function rejectMail($email){
        $details = [
            'title' => 'Mail from Imonetize.com',
            'body' => 'Hi '.$email.' You can not verified this time , please try later'
        ];
        \Mail::to($email)->send(new \App\Mail\rejectMail($details));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $request->validate([
            'password' => 'required',
            'email' => 'required'
        ], [
            'name.required' => 'Name is required',
            'password.required' => 'Password is required'
        ]);

        $email = $request->input('email');
        $password = $request->input('password');
        $check_password = User::where('email',$email)->where('is_active','1')->first();
        if($check_password){
            if(Hash::check($password, $check_password['password'])){
                $user_id = $check_password['id'];
                $request->session()->put('user_id', $user_id);
                return redirect('dashboard');
            }
            else{
                return redirect('login')->with('error','Wrong Password');
            }
        }
        else{
            return redirect('login')->with('error','Invalid User');
        }
    }

    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleProviderCallback()
    {
        // die("sdfgds");
        try {
    
            $user = Socialite::driver('google')->user();
            print_r($user);die();
            $finduser = User::where('social_id', $user->id)->first();
     
            if($finduser){
                $user_id = $finduser['id'];
                $request->session()->put('user_id', $user_id);
                return redirect('dashboard');
                // Auth::login($finduser);
    
                // return redirect('/home');
     
            }else{
                die("sdfgds");
                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'social_id'=> $user->id,
                    'password' => encrypt('123456dummy')
                ]);
                return $newUser;die();
    
                return view('auth.login')->with('success','You are logged in!');
            }
    
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }

    public function dashboard(Request $request){
        $user_id = Session::get('user_id');
        if($user_id){
            $user = User::where('id',$user_id)->first();
            // print_r(json_encode($user));die();
            return view('dashboard')->with('user',$user);
        }
        else{
            return redirect('login')->with('er_status','Session Expired. Please Login again.');
        }
    }

    public function getallusers(Request $request)
    {
        $user_id = Session::get('user_id');
        if($user_id){
            $users = User::where('role','U')->orderBy('id','DESC')->paginate(5);
            $user = User::where('id',$user_id)->first();
            return view('users', ['users'=>$users,'user'=>$user]);
        }
        else{
            return redirect('login')->with('er_status','Session Expired. Please Login again.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function profile(Request $request)
    {
        // die("yehd");
        $user_id = Session::get('user_id');
        if($user_id){
            $user = User::where('id',$user_id)->first();
            return view('profile')->with('user',$user);
        }
        else{
            return redirect('login')->with('er_status','Session Expired. Please Login again.');
        }
    }

    public function commingsoon(Request $request)
    {
        $user_id = Session::get('user_id');
        if($user_id){
            // print_r($user_id);die();
            $user = User::where('id',$user_id)->first();
            return view('commingsoon')->with('user',$user);
        }
        else{
            return redirect('login')->with('er_status','Session Expired. Please Login again.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function editProfile(Request $request)
    {
        $user_id = Session::get('user_id');
        if($user_id){
            $user = User::where('id',$user_id)->first();
            return view('edit_profile')->with('user',$user);
        }
        else{
            return redirect('login')->with('er_status','Session Expired. Please Login again.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */

    public function approveStatus($id)
    {
        $user_id = Session::get('user_id');
        if($user_id){
            $checkstatus = User::where(['id'=>$id])->first();
            $updatestatus = ['is_active' => '1'];
            $update =  User::where(['id'=>$id])->update($updatestatus);
            $users = User::where('role','U')->orderBy('id','DESC')->get();
            $user = User::where('id',$user_id)->first();
            $this->verificationMail($checkstatus->email);
            return redirect('/users')->with('user',$user)->with('users',$users);
        }
        else{
            return redirect('login')->with('er_status','Session Expired. Please Login again.');
        }
    }

    public function rejectStatus($id)
    {
        $user_id = Session::get('user_id');
        if($user_id){
            $checkstatus = User::where(['id'=>$id])->first();
            $updatestatus = ['is_active' => '2'];
            $update =  User::where(['id'=>$id])->update($updatestatus);
            $users = User::where('role','U')->orderBy('id','DESC')->get();
            $user = User::where('id',$user_id)->first();
            $this->rejectMail($checkstatus->email);
            return redirect('/users')->with('user',$user)->with('users',$users);
        }
        else{
            return redirect('login')->with('er_status','Session Expired. Please Login again.');
        }
    }

    public function deleteUser($id)
    {
        $user_id = Session::get('user_id');
        if($user_id){
            $checkstatus = User::where(['id'=>$id])->first();
            // if ($checkstatus['is_active'] == '0') {
            //     $updatestatus = ['is_active' => '1'];
            //     $update =  User::where(['id'=>$id])->update($updatestatus);
            //     $users = User::where('role','U')->orderBy('id','DESC')->get();
            //     $user = User::where('id',$user_id)->first();
            //     return redirect('/users')->with('user',$user)->with('users',$users);
            // }else{
                // $updatestatus = ['is_active' => '0'];
                $update =  User::where(['id'=>$id])->delete();
                $users = User::where('role','U')->orderBy('id','DESC')->get();
                $user = User::where('id',$user_id)->first();
                return redirect('/users')->with('user',$user)->with('users',$users);
            // }
        }
        else{
            return redirect('login')->with('er_status','Session Expired. Please Login again.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        Session::flush();
        return redirect('login');
    }
}
